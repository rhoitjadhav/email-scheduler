import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup,Validators,FormBuilder } from '@angular/forms';
import { AuthService, User } from '../shared/service/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  private subscription:Subscription
  showError:boolean=false;
  errorMsg:string;

  constructor(private _fb:FormBuilder, private _authService:AuthService,private _router:Router) {
    this.formValidation();
   }

  ngOnInit() {
  }

  formValidation(){
    this.loginForm = this._fb.group({
      'username':[null,Validators.compose([Validators.required])],
      'password':[null,Validators.compose([Validators.required,Validators.minLength(8)])]
    });
  }
  
  getLoginDetails(data:User){
    // this.showSpinner= true; 
    this._authService.userLogin(data)
      .subscribe((response:any) =>{
        if(response){
        // Set token to In localstore
        localStorage.setItem('token',response.token);
        // Redirect to the Dashboard Component using Router
        this._router.navigate(['/dashboard']);
        }
      },
      (error) => {
        if (error.status){
          this.showError = true; 
          this.errorMsg = error.error.message;
          setTimeout(() => {
            this.showError = false;
          }, 2000);
        }
        // console.log(error.error.message);
        
        
         
      })
  }

  public ngOnDestroy() {
    if ( this.subscription && this.subscription instanceof Subscription) {
      this.subscription.unsubscribe();
    }
  }

}
