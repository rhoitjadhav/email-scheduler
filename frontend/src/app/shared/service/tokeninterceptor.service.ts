import { Injectable,Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})

export class TokeninterceptorService implements HttpInterceptor {

  constructor(private _intjector:Injector) { }
  intercept(req,next){
    let authService= this._intjector.get(AuthService);
    let tokenizedReq = req.clone({
      setHeader:{
        Authorization: `Bearer ${authService.getToken()}`
      }
    })
    return next.handle(tokenizedReq);
  }
  
}


