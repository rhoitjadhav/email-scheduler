import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface User {
  username:string,
  password:string,
  fistname:string,
  lastname:string,
  publicId:number,
  role:boolean
}

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private url="http://localhost:5000";


  constructor(private _http:HttpClient) {

   }  

  userLogin(userdata:User){
    return this._http.post<User>(this.url + "/sign-in",userdata);
  }
  

n
  getToken(){
    return localStorage.getItem('token');
  }
  
  loggedIn(){
    return !!localStorage.getItem('token')
  }
}
