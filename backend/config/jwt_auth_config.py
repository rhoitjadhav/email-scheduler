# Packages
import jwt
import datetime
from functools import wraps
from flask import request
# Modules
from models.users import app

# Secret Key fot JWT Auth
app.config['SECRET_KEY'] = 'thisissecretkey'


# Generate Token
def generate_token(username):
    token = jwt.encode({
                    'user': username,
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=60)
                }, app.config['SECRET_KEY'])
    return token


# Resource Method Decorator for JWT Auth
def token_required(func):
    @wraps(func)
    def decorated():
        token = request.headers.get('X-Authorization')
        token = token.split()
        token = token[1]

        if not token:
            return ({
                'message': 'Token is missing!'
            }), 403
        try:
            jwt.decode(token, app.config['SECRET_KEY'])
        except:
            return ({
                'message': 'Token is invalid!'
            }), 403
        return func()

    return decorated

