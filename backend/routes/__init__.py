# Packages
import sys
from flask_cors import CORS

# Modules
sys.path.append('./')
from models.users import app, api
from authentication import SignIn, ProfileDetails, Test1


cors = CORS(app, resources={r"/": {"origins": "http://127.0.0.1:4200"}})

# Endpoints
api.add_resource(SignIn, '/sign-in', endpoint='sign_in')
api.add_resource(ProfileDetails, '/profile', endpoint='profile')
api.add_resource(Test1, '/test1', endpoint='test1')


if __name__ == '__main__':
    app.run(debug=True)


