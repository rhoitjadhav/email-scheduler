# Packages
import sys
from flask import request
from flask_restful import Resource

# Modules
sys.path.append('./')
from models.users import Users, app
from config.jwt_auth_config import generate_token, token_required


class SignIn(Resource):
    """ Sign In """
    @staticmethod
    def post():
        username = request.json['username']
        password = request.json['password']

        users = Users.query.filter_by(username=username).first()
        if users is not None:
            if users.username == username and users.password == password:
                token = generate_token(username)

                return ({
                    "statusCode": 0,
                    "message": "Sign In Successfully",
                    "role": users.role,
                    "token": token.decode('UTF-8')
                }), 200
            else:
                return ({
                    "statusCode": 1,
                    "message": "check your username or password"
                }), 401
        else:
            return ({
                "statusCode": 1,
                "message": "check your username or password"
            }), 401


class ProfileDetails(Resource):
    """ Profile Details """
    method_decorators = [token_required]

    @staticmethod
    def post():
        username = request.json['username']

        user = Users.query.filter_by(username=username).first()
        if user is not None:
            return ({
                'statusCode': 0,
                'firstName': user.first_name,
                'lastName': user.last_name,
                'username': user.username,
                'role': user.role,
            }), 200

        return ({
            'statusCode': 1,
            'message': 'Username Not Found'
        }), 401


class Test1(Resource):
    """ Test1 """
    @staticmethod
    def get():
        return ({
            'message': 'Test1 Api'
        })
