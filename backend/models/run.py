""" This file creates table in the database """
# Modules
from users import db

db.create_all()
print("Tables created")
